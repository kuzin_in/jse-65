package ru.kuzin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<TaskDTO> {

    @Nullable
    TaskDTO create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @Nullable
    TaskDTO create(@NotNull String userId, @NotNull String name);

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}