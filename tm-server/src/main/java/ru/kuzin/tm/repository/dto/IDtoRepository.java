package ru.kuzin.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.kuzin.tm.dto.model.AbstractModelDTO;

@NoRepositoryBean
public interface IDtoRepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {
}